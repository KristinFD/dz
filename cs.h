#ifndef CS_H
#define CS_H

#include <QWidget>

namespace Ui {
class CS;
}

class CS : public QWidget
{
    Q_OBJECT

public:
    explicit CS(QWidget *parent = 0);
    ~CS();

private:
    Ui::CS *ui;
};

#endif // CS_H
