/********************************************************************************
** Form generated from reading UI file 'mapform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAPFORM_H
#define UI_MAPFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_mapform
{
public:
    QLabel *label;

    void setupUi(QWidget *mapform)
    {
        if (mapform->objectName().isEmpty())
            mapform->setObjectName(QStringLiteral("mapform"));
        mapform->resize(400, 300);
        label = new QLabel(mapform);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(70, 150, 47, 13));

        retranslateUi(mapform);

        QMetaObject::connectSlotsByName(mapform);
    } // setupUi

    void retranslateUi(QWidget *mapform)
    {
        mapform->setWindowTitle(QApplication::translate("mapform", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("mapform", "maaaap", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class mapform: public Ui_mapform {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAPFORM_H
