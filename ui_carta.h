/********************************************************************************
** Form generated from reading UI file 'carta.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CARTA_H
#define UI_CARTA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>
#include "mapform.h"

QT_BEGIN_NAMESPACE

class Ui_carta
{
public:
    QHBoxLayout *horizontalLayout;
    mapform *widget;
    QLabel *label_2;
    QLabel *label;

    void setupUi(QWidget *carta)
    {
        if (carta->objectName().isEmpty())
            carta->setObjectName(QStringLiteral("carta"));
        carta->resize(400, 300);
        horizontalLayout = new QHBoxLayout(carta);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        widget = new mapform(carta);
        widget->setObjectName(QStringLiteral("widget"));
        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(50, 120, 47, 13));

        horizontalLayout->addWidget(widget);

        label = new QLabel(carta);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);


        retranslateUi(carta);

        QMetaObject::connectSlotsByName(carta);
    } // setupUi

    void retranslateUi(QWidget *carta)
    {
        carta->setWindowTitle(QApplication::translate("carta", "Form", Q_NULLPTR));
        label_2->setText(QApplication::translate("carta", "jgjlkljhjkk", Q_NULLPTR));
        label->setText(QApplication::translate("carta", "carta", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class carta: public Ui_carta {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CARTA_H
