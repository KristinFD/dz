#ifndef MAPFORM_H
#define MAPFORM_H

#include <QWidget>

namespace Ui {
class mapform;
}

class mapform : public QWidget
{
    Q_OBJECT

public:
    explicit mapform(QWidget *parent = 0);
    ~mapform();

private:
    Ui::mapform *ui;
};

#endif // MAPFORM_H
